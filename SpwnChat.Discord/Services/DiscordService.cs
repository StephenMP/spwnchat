﻿using System;
using System.Diagnostics.CodeAnalysis;
using System.Threading.Tasks;
using Discord;
using Discord.Rest;

namespace SpwnChat.Discord.Services
{
    public interface IDiscordService
    {
        bool IsConnected { get; }

        Task DeleteGoLiveMessagesAsync(params IMessage[] messages);

        Task LoginAsync();

        Task LogoutAsync();

        Task<IMessage> PublishGoLiveAsync(string goLiveMessage);

        Task PublishYouTubeVideoMessageAsync(string videoId, string comment);
    }

    public class DiscordService : IDiscordService, IDisposable
    {
        private readonly DiscordRestClient client;
        private ITextChannel customContentChannel;
        private bool disposedValue;
        private ITextChannel publicChatChannel;
        private IGuild server;

        public DiscordService()
        {
            this.client = new DiscordRestClient();
            this.client.LoggedIn += LoggedIn;
            this.client.LoggedOut += LoggedOut;
        }

        public bool IsConnected { get; private set; }

        public async Task DeleteGoLiveMessagesAsync(params IMessage[] messages)
        {
            await this.publicChatChannel.DeleteMessagesAsync(messages).ConfigureAwait(false);
        }

        public void Dispose()
        {
            Dispose(true);
        }

        public async Task LoginAsync()
        {
            await this.client.LoginAsync(TokenType.Bot, "NDA4Nzc3NTAwMDU1OTYxNjAz.DVU_WQ.SKQfLtn2al7POGvsQtCrWDBpCX0").ConfigureAwait(false);
        }

        public async Task LogoutAsync()
        {
            await this.client.LogoutAsync().ConfigureAwait(false);
        }

        public async Task<IMessage> PublishGoLiveAsync(string goLiveMessage)
        {
            return await this.publicChatChannel.SendMessageAsync($"Hey @everyone! {goLiveMessage}").ConfigureAwait(false);
        }

        public async Task PublishYouTubeVideoMessageAsync(string videoId, string comment)
        {
            var shareLink = $"https://youtu.be/{videoId}";
            await this.customContentChannel.SendMessageAsync($"Hey @everyone! New video just went live!\n{comment}\n{shareLink}").ConfigureAwait(false);
        }

        [SuppressMessage("Microsoft.Usage", "CA2213:DisposableFieldsShouldBeDisposed", MessageId = "client")]
        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    this.client?.Dispose();
                }

                disposedValue = true;
            }
        }

        private async Task LoggedIn()
        {
            this.server = await this.client.GetGuildAsync(269890563887595530).ConfigureAwait(false);
            this.publicChatChannel = await this.server.GetTextChannelAsync(269890563887595530).ConfigureAwait(false);
            this.customContentChannel = await this.server.GetTextChannelAsync(399750345422798859).ConfigureAwait(false);
            this.IsConnected = true;
        }

        private async Task LoggedOut()
        {
            await Task.CompletedTask;
            this.IsConnected = false;
        }
    }
}