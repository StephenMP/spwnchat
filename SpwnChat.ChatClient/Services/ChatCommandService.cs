﻿using System;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using SpwnChat.ChatClient.Models;
using SpwnChat.Core.Caching;
using SpwnChat.OBS;
using TwitchLib.Api.Interfaces;
using TwitchLib.Client.Models;

namespace SpwnChat.ChatClient.Services
{
    public interface IChatCommandService
    {
        Task InvokeCommandAsync(ChatCommand chatMessage);
    }

    public class ChatCommandService : IChatCommandService
    {
        private const string CHANNEL_ID = "143488458";
        private const string GIVEAWAY_PATH = @"C:\Users\Stephen\Desktop\SpwnChat\giveaway.txt";
        private static readonly string[] OwnerAndBotsList = new[] { "mrspwn", "botisimo", "pretzelrocks" };

        private readonly ICachingService cachingService;
        private readonly IChatMessagingService chatMessagingService;
        private readonly IMusicService musicService;
        private readonly IOBSClient obsClient;
        private readonly ITwitchAPI twitchAPI;

        public ChatCommandService(ITwitchAPI twitchAPI, IChatMessagingService chatMessagingService, IOBSClient obsClient, IMusicService musicService, ICachingService cachingService)
        {
            this.twitchAPI = twitchAPI;
            this.chatMessagingService = chatMessagingService;
            this.obsClient = obsClient;
            this.musicService = musicService;
            this.cachingService = cachingService;
        }

        public async Task InvokeCommandAsync(ChatCommand chatCommand)
        {
            var command = chatCommand.CommandIdentifier + chatCommand.CommandText;
            var userIsAMod = chatCommand.ChatMessage.IsModerator || chatCommand.ChatMessage.IsBroadcaster;
            var canInvokeCommand = userIsAMod || this.CommandIsReady(command);
            var userIsASubscriber = userIsAMod || chatCommand.ChatMessage.IsSubscriber;

            if (canInvokeCommand)
            {
                // Animation command
                if (this.obsClient.IsAnimationCommand(command))
                {
                    var followsChannel = await this.twitchAPI.V5.Users.UserFollowsChannelAsync(chatCommand.ChatMessage.UserId, CHANNEL_ID);
                    if (followsChannel || userIsAMod)
                    {
                        this.obsClient.PlayAnimation(command, chatCommand.ChatMessage.Username, chatCommand.ChatMessage.IsModerator);
                    }
                    else
                    {
                        await this.chatMessagingService.BroadcastMessageAsync($"Sorry @{chatCommand.ChatMessage.Username}, but animation commands are for followers. But that's easy to fix, just hit the follow button :)");
                    }
                }

                // Subscriber commands
                else if (command.Equals("!" + chatCommand.ChatMessage.Username, StringComparison.OrdinalIgnoreCase) || command.Equals("!" + chatCommand.ChatMessage.DisplayName, StringComparison.OrdinalIgnoreCase))
                {
                    if (userIsASubscriber)
                    {
                        this.obsClient.PlaySubAnimation(chatCommand.ChatMessage.DisplayName);
                        this.SetCommandOnCooldown(command, 86400, userIsAMod);
                    }
                    else
                    {
                        await this.chatMessagingService.BroadcastMessageAsync($"Sorry @{chatCommand.ChatMessage.Username}, but that command is reserved for subs");
                    }
                }
                else
                {
                    switch (command)
                    {
                        case "!clip":
                            await this.HandleClipCommand(chatCommand, userIsAMod);
                            break;

                        case "!shoutout":
                            if (userIsAMod)
                            {
                                await this.HandleShoutoutCommandAsync(chatCommand).ConfigureAwait(false);
                            }
                            break;

                        case "!song":
                            var song = await this.musicService.GetCurrentSongAsync().ConfigureAwait(false);
                            await this.chatMessagingService.BroadcastMessageAsync($"@{chatCommand.ChatMessage.Username} here's the current song: {song}").ConfigureAwait(false);
                            break;

                        case "!live":
                            this.obsClient.SwitchToLiveScene();
                            break;

                        default:
                            break;
                    }
                }
            }
            else
            {
                await this.chatMessagingService.BroadcastMessageAsync($"Sorry @{chatCommand.ChatMessage.Username} the command {command} is on cooldown!").ConfigureAwait(false);
            }
        }

        private static bool UserIsNotABotOrOwner(string username)
        {
            return !OwnerAndBotsList.Contains(username.ToLower());
        }

        private bool CommandIsReady(string command)
        {
            return !this.cachingService.Contains(command);
        }

        private async Task HandleClipCommand(ChatCommand chatCommand, bool userIsAMod)
        {
            try
            {
                using (var client = new HttpClient())
                {
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", "hzq6h9gx82dbb2ralkp4d6dqe5o7hp");
                    using (var response = await client.PostAsync($"https://api.twitch.tv/helix/clips?broadcaster_id={CHANNEL_ID}", new StringContent("{}", Encoding.UTF8, "application/json")))
                    {
                        if (response.IsSuccessStatusCode)
                        {
                            var content = await response.Content.ReadAsStringAsync();
                            var clipResponse = JsonConvert.DeserializeObject<TwitchCreateClipResponse>(content);
                            if (clipResponse != null && clipResponse.CreatedClips.Length > 0)
                            {
                                await this.chatMessagingService.BroadcastMessageAsync($"@{chatCommand.ChatMessage.Username} just created this clip: http://clips.twitch.tv/{clipResponse.CreatedClips[0].Id}");
                            }
                        }
                        else
                        {
                            await this.chatMessagingService.BroadcastMessageAsync("Sorry, but something went wrong when trying to create the clip :(");
                        }
                    }
                }

                this.SetCommandOnCooldown("!clip", 60, userIsAMod);
            }
            catch
            {
                await this.chatMessagingService.BroadcastMessageAsync("Sorry, but something went wrong when trying to create the clip :(");
            }
        }

        private async Task HandleShoutoutCommandAsync(ChatCommand chatCommand)
        {
            if (chatCommand.ArgumentsAsList.Count >= 1)
            {
                var message = string.Empty;
                foreach (var user in chatCommand.ArgumentsAsList.Select(u => u.Replace("@", "").Replace(",", "")))
                {
                    message = $"Sorry, but I couldn't find {user} on twitch!";
                    var twitchUsers = await this.twitchAPI.V5.Users.GetUserByNameAsync(user).ConfigureAwait(false);

                    if (twitchUsers?.Matches.Length > 0)
                    {
                        var twitchUser = twitchUsers.Matches[0];
                        var channel = await this.twitchAPI.V5.Channels.GetChannelByIDAsync(twitchUser.Id);
                        message = $"Make sure you checkout {twitchUser.DisplayName} playing {channel.Game}: {channel.Url}";
                    }

                    await this.chatMessagingService.BroadcastMessageAsync(message).ConfigureAwait(false);
                }
            }
            else
            {
                await this.chatMessagingService.BroadcastMessageAsync("Sorry, you didn't use the command correctly! The command is !shoutout <username>").ConfigureAwait(false);
            }
        }

        private void SetCommandOnCooldown(string command, int timeInSeconds, bool userIsAMod = false)
        {
            if (!userIsAMod)
            {
                this.cachingService.CacheItem(command, command, DateTime.Now.AddSeconds(timeInSeconds));
            }
        }
    }
}