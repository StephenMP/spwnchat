﻿using System.Collections.Generic;
using System.Threading.Tasks;
using SpwnChat.Core.Logging;
using TwitchLib.Client.Interfaces;

namespace SpwnChat.ChatClient.Services
{
    public interface IChatMessagingService
    {
        Task BroadcastMessageAsync(string message);
    }

    public class ChatMessagingService : IChatMessagingService
    {
        private const int MaxCharacterLimit = 500;
        private readonly ILogger logger;
        private readonly ITwitchClient twitchClient;

        public ChatMessagingService(ITwitchClient twitchChatClient, ILogger logger)
        {
            this.twitchClient = twitchChatClient;
            this.logger = logger;
        }

        public async Task BroadcastMessageAsync(string message)
        {
            await Task.CompletedTask;
            message = message.Trim();

            this.LogInfo("Broadcasting message", message);
            foreach (var messageToSend in SplitMessage(message))
            {
                await this.SendMessageAsync(messageToSend).ConfigureAwait(false);
            }
        }

        private static IEnumerable<string> SplitMessage(string message)
        {
            var tempMessage = string.Empty;
            var splitMessages = new List<string>();

            while (message.Length > MaxCharacterLimit)
            {
                tempMessage = $"{message.Substring(0, MaxCharacterLimit - 3)}...";
                splitMessages.Add(tempMessage.Trim());
                message = message.Substring(tempMessage.Length - 3);
            }

            splitMessages.Add(message.Trim());
            return splitMessages;
        }

        private void LogInfo(string message, object data = null)
        {
            this.logger.Info(this, message, data);
        }

        private async Task SendMessageAsync(string message)
        {
            await Task.CompletedTask;
            this.twitchClient.SendMessage("mrspwn", message);
        }
    }
}