﻿using System;
using System.Net.Http;
using System.Runtime.InteropServices;
using System.Threading.Tasks;

namespace SpwnChat.ChatClient.Services
{
    public interface IMusicService
    {
        Task<string> GetCurrentSongAsync();

        void NextSong();

        void PlayOrPauseMusic();

        Task MuteMusicAsync();

        Task UnmuteMusicAsync();

        bool Muted { get; }
    }

    public class MusicService : IMusicService
    {
        private const int KEYEVENT_KEYDOWN = 0x0001;
        private const int KEYEVENT_KEYUP = 0x0002;
        private const int VK_MEDIA_NEXT_TRACK = 0xB0;
        private const int VK_MEDIA_PLAY_PAUSE = 0xB3;

        //private const int VK_MEDIA_PREV_TRACK = 0xB1;

        public MusicService()
        {
        }

        public bool Muted { get; private set; }

        public async Task<string> GetCurrentSongAsync()
        {
            try
            {
                using (var client = new HttpClient())
                {
                    const string url = "https://www.pretzel.rocks/api/v1/playing/twitch/mrspwn";
                    return await client.GetStringAsync(url).ConfigureAwait(false);
                }
            }
            catch
            {
                return "Sorry, I couldn't get the current song";
            }
        }

        public async Task MuteMusicAsync()
        {
            this.Muted = true;
        }

        public void NextSong()
        {
            NativeMethods.keybd_event(VK_MEDIA_NEXT_TRACK, 0, KEYEVENT_KEYDOWN, IntPtr.Zero);
            NativeMethods.keybd_event(VK_MEDIA_NEXT_TRACK, 0, KEYEVENT_KEYUP, IntPtr.Zero);
        }

        public void PlayOrPauseMusic()
        {
            NativeMethods.keybd_event(VK_MEDIA_PLAY_PAUSE, 0, KEYEVENT_KEYDOWN, IntPtr.Zero);
            NativeMethods.keybd_event(VK_MEDIA_PLAY_PAUSE, 0, KEYEVENT_KEYUP, IntPtr.Zero);
        }

        public async Task UnmuteMusicAsync()
        {
            this.Muted = false;
        }
    }

    internal static class NativeMethods
    {
        [DllImport("user32.dll", SetLastError = true)]
#pragma warning disable IDE1006 // Naming Styles
        public static extern void keybd_event(byte virtualKey, byte scanCode, uint flags, IntPtr extraInfo);

#pragma warning restore IDE1006 // Naming Styles
    }
}