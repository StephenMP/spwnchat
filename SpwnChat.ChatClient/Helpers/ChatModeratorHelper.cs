﻿using System.IO;
using System.Linq;

namespace SpwnChat.ChatClient.Helpers
{
    internal sealed class ChatModeratorHelper
    {
        private static ChatModeratorHelper current;
        private readonly string[] mods;

        private ChatModeratorHelper()
        {
            this.mods = File.ReadAllLines(@"C:\Users\Stephen\Desktop\SpwnChat\mods.txt");
        }

        public static ChatModeratorHelper Current
        {
            get
            {
                current = current ?? new ChatModeratorHelper();
                return current;
            }
        }

        public bool UserIsAModerator(string username)
        {
            if (!string.IsNullOrWhiteSpace(username))
            {
                return this.mods.Contains(username.ToLower());
            }

            return false;
        }
    }
}