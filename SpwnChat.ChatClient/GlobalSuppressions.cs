using System.Diagnostics.CodeAnalysis;

[assembly: SuppressMessage("Microsoft.Design", "CA1009:DeclareEventHandlersCorrectly", Scope = "member", Target = "SpwnChat.ChatClient.Clients.Mixer.IMixerChatClient.#OnConnected")]
[assembly: SuppressMessage("Microsoft.Design", "CA1009:DeclareEventHandlersCorrectly", Scope = "member", Target = "SpwnChat.ChatClient.Clients.Youtube.IYoutubeChatClient.#OnConnected")]
[assembly: SuppressMessage("Microsoft.Design", "CA1009:DeclareEventHandlersCorrectly", Scope = "member", Target = "SpwnChat.ChatClient.Clients.Mixer.IMixerChatClient.#OnDisconnected")]
[assembly: SuppressMessage("Microsoft.Design", "CA1009:DeclareEventHandlersCorrectly", Scope = "member", Target = "SpwnChat.ChatClient.Clients.Youtube.IYoutubeChatClient.#OnDisconnected")]
[assembly: SuppressMessage("Microsoft.Design", "CA1009:DeclareEventHandlersCorrectly", Scope = "member", Target = "SpwnChat.ChatClient.Clients.Mixer.IMixerChatClient.#OnError")]
[assembly: SuppressMessage("Microsoft.Design", "CA1009:DeclareEventHandlersCorrectly", Scope = "member", Target = "SpwnChat.ChatClient.Clients.Youtube.IYoutubeChatClient.#OnMessageReceived")]
// This file is used by Code Analysis to maintain SuppressMessage
// attributes that are applied to this project.
// Project-level suppressions either have no target or are given
// a specific target and scoped to a namespace, type, member, etc.
//
// To add a suppression to this file, right-click the message in the
// Code Analysis results, point to "Suppress Message", and click
// "In Suppression File".
// You do not need to add suppressions to this file manually.