﻿using System.Collections.Concurrent;
using System.IO;
using System.Threading;
using System.Threading.Tasks;
using SpwnChat.ChatClient.Services;
using SpwnChat.Core.Logging;
using TwitchLib.Api.Interfaces;
using TwitchLib.Client.Events;
using TwitchLib.Client.Interfaces;
using TwitchLib.Client.Models;
using TwitchLib.Communication.Events;

namespace SpwnChat.ChatClient
{
    public interface IChatBotClient
    {
        bool IsConnected { get; }

        void Connect();

        void Disconnect();
    }

    public class ChatBotClient : IChatBotClient
    {
        private readonly IChatCommandService chatCommandService;
        private readonly IChatMessagingService chatMessagingService;
        private readonly ILogger logger;
        private readonly BlockingCollection<OnChatCommandReceivedArgs> messageQueue;
        private readonly ITwitchAPI twitchApi;
        private readonly ITwitchClient twitchClient;
        private bool disconnectSent;
        private Task handleMessageEventTask;
        private Timer timer;
        private int timerMessageIndex;
        private string[] timerMessages;
        private bool userHasChatted;

        public ChatBotClient
        (
            ITwitchClient twitchChatClient,
            IChatCommandService chatCommandService,
            IChatMessagingService chatMessagingService,
            ITwitchAPI twitchAPI,
            ILogger logger
        )
        {
            this.messageQueue = new BlockingCollection<OnChatCommandReceivedArgs>();
            this.twitchClient = twitchChatClient;
            this.chatCommandService = chatCommandService;
            this.chatMessagingService = chatMessagingService;
            this.twitchApi = twitchAPI;
            this.logger = logger;
        }

        public bool IsConnected { get; private set; }

        public void Connect()
        {
            this.LogInfo("Connecting chat client");

            // Connect the chat bot clients
            this.ConnectTwitchClient();

            // Start the message queue consumer
            this.disconnectSent = false;
            this.handleMessageEventTask = new Task(async () => await HandleCommandEventAsync().ConfigureAwait(false), TaskCreationOptions.LongRunning);
            this.handleMessageEventTask.Start();
            this.LogInfo("Message queue consumer started");

            // Start the timer for timed messages
            this.timer = new Timer(PostMessage, null, 600000, 600000);
            this.timerMessages = File.ReadAllLines(@"C:\Users\Stephen\Desktop\SpwnChat\timerMessages.txt");
            this.timerMessageIndex = 0;
            this.LogInfo("Timer started");

            this.IsConnected = true;
        }

        public void Disconnect()
        {
            this.LogInfo("Disconnecting chat client");

            this.disconnectSent = true;

            this.DisconnectTwitchClient();

            this.timer.Dispose();

            this.IsConnected = false;
        }

        private void ConnectTwitchClient()
        {
            this.LogInfo("Connecting Twitch client");
            this.twitchClient.OnChatCommandReceived += OnTwitchChatCommandReceived;
            this.twitchClient.OnJoinedChannel += OnTwitchJoinedChannel;
            this.twitchClient.OnConnected += OnTwitchConnected;
            this.twitchClient.OnDisconnected += OnTwitchDisconnected;
            this.twitchClient.OnNewSubscriber += OnTwitchNewSubscriber;
            this.twitchClient.OnReSubscriber += OnTwitchReSubscriber;
            this.twitchClient.OnMessageReceived += OnTwitchMessageReceived;

            this.twitchClient.Connect();
            while (!this.twitchClient.IsConnected)
            {
                Thread.Sleep(100);
            }
        }

        private void DisconnectTwitchClient()
        {
            if (this.twitchClient.IsConnected)
            {
                this.LogInfo("Disconnecting Twitch client");
                this.twitchClient.OnChatCommandReceived -= OnTwitchChatCommandReceived;
                this.twitchClient.OnJoinedChannel -= OnTwitchJoinedChannel;
                this.twitchClient.OnConnected -= OnTwitchConnected;
                this.twitchClient.OnDisconnected -= OnTwitchDisconnected;
                this.twitchClient.OnNewSubscriber -= OnTwitchNewSubscriber;
                this.twitchClient.OnReSubscriber -= OnTwitchReSubscriber;
                this.twitchClient.OnMessageReceived -= OnTwitchMessageReceived;

                this.twitchClient.Disconnect();
                while (this.twitchClient.IsConnected)
                {
                    Thread.Sleep(100);
                }
            }
        }

        private async Task HandleCommandEventAsync()
        {
            this.LogInfo($"{nameof(HandleCommandEventAsync)} was started successfully");

            while (!this.disconnectSent)
            {
                while (!this.messageQueue.IsCompleted)
                {
                    var eventArgs = this.messageQueue.Take();
                    await this.HandleCommandReceivedAsync(eventArgs.Command).ConfigureAwait(false);
                }
            }

            this.LogInfo($"{nameof(HandleCommandEventAsync)} was cancelled successfully");
        }

        private async Task HandleCommandReceivedAsync(ChatCommand chatCommand)
        {
            this.LogInfo("Handling message received", new { chatCommand.ChatMessage.Username, chatCommand.ChatMessage.Message });

            this.LogInfo("Invoking command", chatCommand);
            await this.chatCommandService.InvokeCommandAsync(chatCommand).ConfigureAwait(false);
        }

        private void LogInfo(string message, object data = null)
        {
            this.logger?.Info(this, message, data);
        }

        private void OnTwitchChatCommandReceived(object sender, OnChatCommandReceivedArgs e)
        {
            this.LogInfo($"Received command", e);
            this.messageQueue.Add(e);
        }

        private void OnTwitchConnected(object sender, OnConnectedArgs e)
        {
            this.LogInfo($"Twitch chat client connected");
        }

        private void OnTwitchDisconnected(object sender, OnDisconnectedEventArgs e)
        {
            this.LogInfo($"Twitch chat client disconnected");
        }

        private void OnTwitchJoinedChannel(object sender, OnJoinedChannelArgs e)
        {
            this.LogInfo($"Twitch chat client joined channel {e.Channel}");
        }

        private void OnTwitchMessageReceived(object sender, OnMessageReceivedArgs e)
        {
            this.userHasChatted = true;
        }

        private void OnTwitchNewSubscriber(object sender, OnNewSubscriberArgs e)
        {
            this.twitchClient.SendMessage("mrspwn", $"Spam hearts and/or Spwn Loves for {e?.Subscriber?.DisplayName} for the sub! mrspwnLove <3 mrspwnLove <3 mrspwnLove <3 mrspwnLove <3 mrspwnLove");
        }

        private void OnTwitchReSubscriber(object sender, OnReSubscriberArgs e)
        {
            this.twitchClient.SendMessage("mrspwn", $"Spam hearts and/or Spwn Loves for {e?.ReSubscriber?.DisplayName} for the sub! mrspwnLove <3 mrspwnLove <3 mrspwnLove <3 mrspwnLove <3 mrspwnLove");
        }

        private async void PostMessage(object state)
        {
            if (userHasChatted)
            {
                await this.chatMessagingService.BroadcastMessageAsync(this.timerMessages[this.timerMessageIndex++]);
                this.timerMessageIndex = this.timerMessageIndex % this.timerMessages.Length;
                this.userHasChatted = false;
            }
        }
    }
}