﻿using Newtonsoft.Json;

namespace SpwnChat.ChatClient.Models
{
    public class TwitchCreateClipResponse
    {
        [JsonProperty(PropertyName = "data")]
        public TwitchCreatedClip[] CreatedClips { get; protected set; }
    }

    public class TwitchCreatedClip
    {
        [JsonProperty(PropertyName = "edit_url")]
        public string EditUrl { get; protected set; }

        [JsonProperty(PropertyName = "id")]
        public string Id { get; protected set; }
    }
}