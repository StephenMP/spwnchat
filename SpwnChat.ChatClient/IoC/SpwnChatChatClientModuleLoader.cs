﻿using Google.Apis.Util.Store;
using LightInject;
using SpwnChat.ChatClient.Services;
using SpwnChat.Core.Caching;
using SpwnChat.Core.IoC;
using SpwnChat.Core.Logging;
using SpwnChat.Repository;

using TwitchLib.Api;
using TwitchLib.Api.Interfaces;
using TwitchLib.Client;
using TwitchLib.Client.Interfaces;
using TwitchLib.Client.Models;

namespace SpwnChat.ChatClient.IoC
{
    public class ChatBotModuleLoader : IModuleLoader
    {
        private const string TwitchAccessToken = "hzq6h9gx82dbb2ralkp4d6dqe5o7hp";
        private const string TwitchRefreshToken = "ga2ohs0jrjap9u0lwlt21pqjrkmvcqqvqbr5w0rq2c8ubfv5x2";

        public void RegisterServices(IServiceContainer serviceContainer)
        {
            // Register transient services
            serviceContainer.Register<ILogger, Logger>();
            serviceContainer.Register<IMusicService, MusicService>();
            serviceContainer.Register<IDataStore, YoutubeCredentialDataStore>();
            serviceContainer.Register<IChatCommandService, ChatCommandService>();
            serviceContainer.Register<ICachingService, CachingService>();

            // Register singleton instances
            serviceContainer.RegisterSingleton(_ => BuildTwitchApi());
            serviceContainer.RegisterSingleton(_ => BuildTwitchClient());
            serviceContainer.RegisterSingleton<IChatMessagingService, ChatMessagingService>();
        }

        private ITwitchAPI BuildTwitchApi()
        {
            var twitchApi = new TwitchAPI();
            twitchApi.Settings.ClientId = "r3vnlefpb023sp33xhlmt829v4hwsg";
            twitchApi.Settings.Secret = "hn7t3ff7pz74iohmqdkyqf3tk8yvrn";
            twitchApi.Settings.AccessToken = TwitchAccessToken;

            return twitchApi;
        }

        private ITwitchClient BuildTwitchClient()
        {
            var twitchClient = new TwitchClient();
            var creds = new ConnectionCredentials("mrspwn", $"oauth:{TwitchAccessToken}");
            twitchClient.Initialize(creds, "mrspwn");

            return twitchClient;
        }
    }
}