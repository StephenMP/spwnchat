﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Threading;
using Newtonsoft.Json;
using OBSWebsocketDotNet;
using SpwnChat.Core.Caching;
using SpwnChat.OBS.Models;

namespace SpwnChat.OBS
{
    public interface IOBSClient
    {
        bool IsConnected { get; set; }

        void Connect();

        void Disconnect();

        bool IsAnimationCommand(string command);

        void PlayAnimation(string command, string user, bool userIsMod);

        void PlaySubAnimation(string username);

        void ShowAnimation(IEnumerable<string> animationSources, int videoLengthMilliseconds);

        void SwitchToLiveScene();

        void ToggleSource(string animationSource, bool visible);
    }

    public class OBSClient : IOBSClient
    {
        private const string ANIMATION_BASE_SCENE = "Base - Video - Animations";
        private const string GAME_CAPTURE_SCENE = "Scene - Primary - Game Capture";
        private readonly IDictionary<string, AnimationConfiguration> animationConfigurationsByCommand;
        private readonly ICachingService cachingService;
        private readonly OBSWebsocket obsWebSocket;

        public OBSClient(ICachingService cachingService)
        {
            this.animationConfigurationsByCommand = new Dictionary<string, AnimationConfiguration>();
            this.obsWebSocket = new OBSWebsocket();
            this.obsWebSocket.Connected += ObsOnConnect;
            this.obsWebSocket.Disconnected += ObsOnDisconnect;
            this.cachingService = cachingService;
        }

        public bool IsConnected { get; set; }

        public void Connect()
        {
            this.LoadAnimationConfigs();
            this.obsWebSocket.Connect("ws://localhost:4444", "SomeStrongPassword");
        }

        public void Disconnect()
        {
            this.animationConfigurationsByCommand.Clear();
            this.obsWebSocket.Disconnect();
        }

        public bool IsAnimationCommand(string command)
        {
            return this.animationConfigurationsByCommand.ContainsKey(command);
        }

        public void PlayAnimation(string command, string user, bool userIsMod)
        {
            if (this.animationConfigurationsByCommand.ContainsKey(command))
            {
                var animationConfig = this.animationConfigurationsByCommand[command];
                this.ShowAnimation(animationConfig.AnimationResources, animationConfig.AnimationDelay);
                PutAnimationOnCooldown(command, user, animationConfig.CooldownMilliseconds);
            }
        }

        public void PlaySubAnimation(string username)
        {
            const string subscriberVideoSourceName = "Video - Subscriber";
            const string subscriberTextSourceName = "Text - Subscriber";
            var text = $"{username}\nis soooooooo\nAWESOME!";

            // Write text to file to be used for subscribers
            File.WriteAllText(@"D:\Streaming\Labels\sub_animation.txt", text);
            Thread.Sleep(1000);

            // Display subscriber animation
            this.ToggleSource(subscriberVideoSourceName, true);
            Thread.Sleep(1000);
            this.ToggleSource(subscriberTextSourceName, true);

            // Wait for video
            Thread.Sleep(9000);

            // Hide subscriber animation
            this.ToggleSource(subscriberVideoSourceName, false);
            this.ToggleSource(subscriberTextSourceName, false);
        }

        public void ShowAnimation(IEnumerable<string> animationSources, int videoLengthMilliseconds)
        {
            var scene = this.obsWebSocket.GetCurrentScene();
            if (scene.Name.Equals(GAME_CAPTURE_SCENE, StringComparison.OrdinalIgnoreCase))
            {
                this.obsWebSocket.SetSourceRender(ANIMATION_BASE_SCENE, true);
                foreach (var animationSource in animationSources)
                {
                    this.obsWebSocket.SetSourceRender(animationSource, true, ANIMATION_BASE_SCENE);
                }

                Thread.Sleep(videoLengthMilliseconds + 1000);

                foreach (var animationSource in animationSources)
                {
                    this.obsWebSocket.SetSourceRender(animationSource, false, ANIMATION_BASE_SCENE);
                }
                this.obsWebSocket.SetSourceRender(ANIMATION_BASE_SCENE, false);
            }
        }

        public void SwitchToLiveScene()
        {
            var currentScene = this.obsWebSocket.GetCurrentScene();
            if (currentScene.Name.ToLower().Contains("intermission"))
            {
                this.obsWebSocket.SetCurrentScene(GAME_CAPTURE_SCENE);
            }
        }

        public void ToggleSource(string animationSource, bool visible)
        {
            var scene = this.obsWebSocket.GetCurrentScene();
            if (scene.Name.Equals(GAME_CAPTURE_SCENE, StringComparison.OrdinalIgnoreCase))
            {
                this.obsWebSocket.SetSourceRender(ANIMATION_BASE_SCENE, visible);
                this.obsWebSocket.SetSourceRender(animationSource, visible, ANIMATION_BASE_SCENE);
            }
        }

        private void LoadAnimationConfigs()
        {
            const string animationsConfigPath = @"C:\Users\Stephen\Desktop\SpwnChat\animations.json";
            var animationConfigsJson = File.ReadAllText(animationsConfigPath);
            var animationConfigs = JsonConvert.DeserializeObject<List<AnimationConfiguration>>(animationConfigsJson);
            foreach (var config in animationConfigs)
            {
                this.animationConfigurationsByCommand.Add(config.AnimationCommand, config);
            }
        }

        private void ObsOnConnect(object sender, EventArgs e)
        {
            this.IsConnected = true;
        }

        private void ObsOnDisconnect(object sender, EventArgs e)
        {
            this.IsConnected = false;
        }

        private void PutAnimationOnCooldown(string command, string user, long cooldownMilliseconds)
        {
            if (!string.Equals(user, "mrspwn", StringComparison.OrdinalIgnoreCase))
            {
                this.cachingService.CacheItem(command, command, DateTime.Now.AddMilliseconds(cooldownMilliseconds));
            }
        }
    }
}