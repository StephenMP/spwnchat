﻿using System.Collections.Generic;

namespace SpwnChat.OBS.Models
{
    internal class AnimationConfiguration
    {
        public string AnimationCommand { get; set; }
        public int AnimationDelay { get; set; }
        public IEnumerable<string> AnimationResources { get; set; }
        public long CooldownMilliseconds { get; set; }
    }
}