﻿using LightInject;
using SpwnChat.Core.IoC;

namespace SpwnChat.OBS.IoC
{
    public class SpwnChatOBSClientModuleLoader : IModuleLoader
    {
        public void RegisterServices(IServiceContainer serviceContainer)
        {
            serviceContainer.RegisterSingleton<IOBSClient, OBSClient>();
        }
    }
}