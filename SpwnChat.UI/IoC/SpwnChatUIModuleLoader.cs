﻿using LightInject;
using SpwnChat.ChatClient;
using SpwnChat.Core.Caching;
using SpwnChat.Core.IoC;
using SpwnChat.Core.Logging;
using SpwnChat.Discord.Services;
using SpwnChat.StreamLabs.Clients;
using SpwnChat.UI.Forms;

namespace SpwnChat.UI.IoC
{
    internal class SpwnChatUIModuleLoader : IModuleLoader
    {
        public void RegisterServices(IServiceContainer serviceContainer)
        {
            serviceContainer.Register<ILogger, Logger>();
            serviceContainer.Register<IStreamLabsClient, StreamLabsClient>();
            serviceContainer.Register<IDiscordService, DiscordService>();
            serviceContainer.Register<ICachingService, CachingService>();

            serviceContainer.RegisterSingleton<IChatBotClient, ChatBotClient>();
            serviceContainer.Register<MainForm>();
        }
    }
}