﻿using System;
using System.Threading.Tasks;
using System.Windows.Forms;
using SpwnChat.ChatClient;
using SpwnChat.Core.Logging;
using SpwnChat.Discord.Services;
using SpwnChat.OBS;

namespace SpwnChat.UI.Forms
{
    internal partial class MainForm : Form
    {
        private readonly IChatBotClient chatBotClient;
        private readonly ConnectionState connectionState;
        private readonly IDiscordService discordService;
        private readonly ILogger logger;
        private readonly IOBSClient obsClient;

        public MainForm(IDiscordService discordService, IChatBotClient chatBotClient, IOBSClient obsClient, ILogger logger)
        {
            InitializeComponent();
            InitializeAdditionalEventHandlers();

            this.connectionState = new ConnectionState();
            this.obsClient = obsClient;
            this.discordService = discordService;
            this.chatBotClient = chatBotClient;
            this.logger = logger;
        }

        public async void HandleClickEvent(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;

            // Primary button click handle action
            if (sender == this.btnBotConnect)
            {
                BtnBotConnect_Click();
            }
            else if (sender == this.btnOBSConnect)
            {
                BtnOBSConnect_Click();
            }
            else if (sender == this.btnDiscordAnnounceLive)
            {
                await BtnDiscordAnnounceLive_ClickAsync().ConfigureAwait(true);
            }
            else if (sender == this.btnEverythingConnect)
            {
                await BtnEverythingConnect_ClickAsync().ConfigureAwait(true);
                Cursor.Current = Cursors.Arrow;
            }

            // Secondary handle action
            if (this.connectionState.SomethingIsConnected)
            {
                if (this.btnEverythingConnect.Text.Contains("Connect"))
                {
                    this.btnEverythingConnect.Enabled = false;
                }
            }
            else
            {
                this.btnEverythingConnect.Enabled = true;
            }

            Cursor.Current = Cursors.Arrow;
        }

        public void HandleFormClosing(object sender, FormClosingEventArgs e)
        {
            if (this.connectionState.SomethingIsConnected)
            {
                var dialogResult = MessageBox.Show("You still have services connected, are you sure you want to close?", "Are You Sure?", MessageBoxButtons.YesNo);
                e.Cancel = dialogResult == DialogResult.No;
            }

            this.logger?.Info(this, $"=======================");
            this.logger?.Info(this, $"Session Done: {DateTime.Now.ToShortTimeString()}");
            this.logger?.Info(this, $"=======================");
            this.logger?.CloseAndFlush();
        }

        private void BtnBotConnect_Click()
        {
            if (this.connectionState.ChatBotIsConnected)
            {
                this.chatBotClient.Disconnect();
                this.UpdateButton(this.btnBotConnect, ConnectionState.ConnectBotLabel);
            }
            else
            {
                this.chatBotClient.Connect();
                this.UpdateButton(this.btnBotConnect, ConnectionState.DisconnectBotLabel, System.Drawing.Color.LightGreen);
            }

            this.connectionState.ChatBotIsConnected = !this.connectionState.ChatBotIsConnected;
        }

        private async Task BtnDiscordAnnounceLive_ClickAsync()
        {
            if (!this.discordService.IsConnected)
            {
                await this.discordService.LoginAsync().ConfigureAwait(true);
            }

            if (this.connectionState.DiscordAnnouncementMessage != null)
            {
                this.UpdateButton(this.btnDiscordAnnounceLive, ConnectionState.AnnounceOnDiscordLabel);
                await this.discordService.DeleteGoLiveMessagesAsync(this.connectionState.DiscordAnnouncementMessage).ConfigureAwait(true);
                this.connectionState.DiscordAnnouncementMessage = null;
            }
            else
            {
                this.UpdateButton(this.btnDiscordAnnounceLive, ConnectionState.DeleteAnnounceOnDiscordLabel, System.Drawing.Color.LightGreen);
                this.connectionState.DiscordAnnouncementMessage = await this.discordService.PublishGoLiveAsync("I just went live!\nhttps://www.twitch.tv/mrspwn").ConfigureAwait(true);
            }
        }

        private async Task BtnEverythingConnect_ClickAsync()
        {
            if (this.connectionState.EverythingIsConnected)
            {
                this.btnEverythingConnect.Text = ConnectionState.ConnectEverythingLabel;
                this.btnBotConnect.Enabled = true;
                this.btnOBSConnect.Enabled = true;
                this.btnDiscordAnnounceLive.Enabled = true;
            }
            else
            {
                this.btnEverythingConnect.Text = ConnectionState.DisconnectEverythingLabel;
                this.btnBotConnect.Enabled = false;
                this.btnOBSConnect.Enabled = false;
                this.btnDiscordAnnounceLive.Enabled = false;
            }

            BtnBotConnect_Click();
            BtnOBSConnect_Click();
            await BtnDiscordAnnounceLive_ClickAsync().ConfigureAwait(true);
        }

        private void BtnOBSConnect_Click()
        {
            if (this.connectionState.LabelsIsConnected)
            {
                if (this.obsClient.IsConnected)
                {
                    this.obsClient.Disconnect();
                }

                this.UpdateButton(this.btnOBSConnect, ConnectionState.ConnectOBSLabel);
            }
            else
            {
                if (!this.obsClient.IsConnected)
                {
                    this.obsClient.Connect();
                }

                this.UpdateButton(this.btnOBSConnect, ConnectionState.DisconnectLabelsLabel, System.Drawing.Color.LightGreen);
            }

            this.connectionState.LabelsIsConnected = !this.connectionState.LabelsIsConnected;
        }

        private void InitializeAdditionalEventHandlers()
        {
            this.FormClosing += HandleFormClosing;

            this.btnBotConnect.Click += HandleClickEvent;
            this.btnOBSConnect.Click += HandleClickEvent;
            this.btnDiscordAnnounceLive.Click += HandleClickEvent;
            this.btnEverythingConnect.Click += HandleClickEvent;
        }

        private void UpdateButton(Button button, string text)
        {
            this.UpdateButton(button, text, System.Drawing.SystemColors.Control);
        }

        private void UpdateButton(Button button, string text, System.Drawing.Color color)
        {
            button.Text = text;
            button.BackColor = color;
        }
    }
}