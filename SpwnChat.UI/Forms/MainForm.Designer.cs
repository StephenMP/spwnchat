﻿namespace SpwnChat.UI.Forms
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.btnBotConnect = new System.Windows.Forms.Button();
            this.btnOBSConnect = new System.Windows.Forms.Button();
            this.btnDiscordAnnounceLive = new System.Windows.Forms.Button();
            this.btnEverythingConnect = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btnBotConnect
            // 
            this.btnBotConnect.Location = new System.Drawing.Point(13, 13);
            this.btnBotConnect.Name = "btnBotConnect";
            this.btnBotConnect.Size = new System.Drawing.Size(75, 75);
            this.btnBotConnect.TabIndex = 0;
            this.btnBotConnect.Text = "Connect Bot";
            this.btnBotConnect.UseVisualStyleBackColor = true;
            // 
            // btnOBSConnect
            // 
            this.btnOBSConnect.Location = new System.Drawing.Point(95, 13);
            this.btnOBSConnect.Name = "btnOBSConnect";
            this.btnOBSConnect.Size = new System.Drawing.Size(75, 75);
            this.btnOBSConnect.TabIndex = 1;
            this.btnOBSConnect.Text = "Connect OBS";
            this.btnOBSConnect.UseVisualStyleBackColor = true;
            // 
            // btnDiscordAnnounceLive
            // 
            this.btnDiscordAnnounceLive.Location = new System.Drawing.Point(177, 13);
            this.btnDiscordAnnounceLive.Name = "btnDiscordAnnounceLive";
            this.btnDiscordAnnounceLive.Size = new System.Drawing.Size(75, 75);
            this.btnDiscordAnnounceLive.TabIndex = 2;
            this.btnDiscordAnnounceLive.Text = "Announce Live in Discord";
            this.btnDiscordAnnounceLive.UseVisualStyleBackColor = true;
            // 
            // btnEverythingConnect
            // 
            this.btnEverythingConnect.Location = new System.Drawing.Point(259, 13);
            this.btnEverythingConnect.Name = "btnEverythingConnect";
            this.btnEverythingConnect.Size = new System.Drawing.Size(75, 75);
            this.btnEverythingConnect.TabIndex = 3;
            this.btnEverythingConnect.Text = "Connect Everything";
            this.btnEverythingConnect.UseVisualStyleBackColor = true;
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(346, 101);
            this.Controls.Add(this.btnEverythingConnect);
            this.Controls.Add(this.btnDiscordAnnounceLive);
            this.Controls.Add(this.btnOBSConnect);
            this.Controls.Add(this.btnBotConnect);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "MainForm";
            this.Text = "SpwnBot";
            this.ResumeLayout(false);

        }

        #endregion

        internal System.Windows.Forms.Button btnBotConnect;
        internal System.Windows.Forms.Button btnOBSConnect;
        internal System.Windows.Forms.Button btnDiscordAnnounceLive;
        internal System.Windows.Forms.Button btnEverythingConnect;
    }
}

