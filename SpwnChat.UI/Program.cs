﻿using System;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using SpwnChat.Core.IoC;
using SpwnChat.Core.Logging;
using SpwnChat.Core.Logging.Enums;
using SpwnChat.Core.Mapping;
using SpwnChat.UI.Forms;

namespace SpwnChat
{
    internal static class Program
    {
        [STAThread]
        private static void Main()
        {
            MainAsync().GetAwaiter().GetResult();
        }

        private static async Task MainAsync()
        {
            await Task.CompletedTask;

            // Setup handlers for unhandled exceptions
            //AppDomain.CurrentDomain.FirstChanceException += FirstChanceExceptionHandler;
            Application.ThreadException += ThreadExceptionHandler;
            AppDomain.CurrentDomain.UnhandledException += UnhandledExceptionHandler;

            // Log session start
            Logger.WriteLog("Program", LogLevel.Info, $"=======================");
            Logger.WriteLog("Program", LogLevel.Info, $"Session Start: {DateTime.Now.ToShortTimeString()}");
            Logger.WriteLog("Program", LogLevel.Info, $"=======================");

            // Register services and mappings
            ServiceLocator.Current.RegisterAllAssemblyModules();
            Mapper.Current.RegisterAllAssemblyMappingConfigurations();

            // Application startup
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(ServiceLocator.Current.Resolve<MainForm>());
        }

        //private static void FirstChanceExceptionHandler(object sender, FirstChanceExceptionEventArgs e)
        //{
        //    Logger.WriteLog(sender.GetType().Name, LogLevel.Error, e.Exception.ToString());
        //}

        private static void ThreadExceptionHandler(object sender, ThreadExceptionEventArgs e)
        {
            Logger.WriteLog(sender.GetType().Name, LogLevel.Error, e.Exception.ToString());
            MessageBox.Show("Something went wrong and broke the bot :(");
        }

        private static void UnhandledExceptionHandler(object sender, UnhandledExceptionEventArgs e)
        {
            var exception = e.ExceptionObject as Exception;
            Logger.WriteLog(sender.GetType().Name, LogLevel.Error, exception.ToString());
            MessageBox.Show("Something went wrong and broke the bot :(");
        }
    }
}