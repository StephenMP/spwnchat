﻿using Discord;

namespace SpwnChat.UI
{
    internal class ConnectionState
    {
        public const string AnnounceOnDiscordLabel = "Announce Live in Discord";
        public const string ConnectBotLabel = "Connect Bot";
        public const string ConnectEverythingLabel = "Connect Everything";
        public const string ConnectOBSLabel = "Connect OBS";
        public const string DeleteAnnounceOnDiscordLabel = "Delete Announcement in Discord";
        public const string DisconnectBotLabel = "Disconnect Bot";
        public const string DisconnectEverythingLabel = "Disconnect Everything";
        public const string DisconnectLabelsLabel = "Disconnect OBS";
        public bool ChatBotIsConnected { get; set; }
        public IMessage DiscordAnnouncementMessage { get; set; }
        public bool EverythingIsConnected => this.ChatBotIsConnected && this.LabelsIsConnected && this.DiscordAnnouncementMessage != null;
        public bool LabelsIsConnected { get; set; }
        public bool SomethingIsConnected => this.ChatBotIsConnected || this.LabelsIsConnected || this.DiscordAnnouncementMessage != null;
    }
}