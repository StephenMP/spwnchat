﻿using System.Threading.Tasks;
using Google.Apis.Util.Store;
using LiteDB;
using Newtonsoft.Json;
using SpwnChat.Repository.Entities;

namespace SpwnChat.Repository
{
    public class YoutubeCredentialDataStore : IDataStore
    {
        public async Task ClearAsync()
        {
            using (var database = Database())
            {
                var collection = database.DropCollection("YoutubeCredential");
                await Task.CompletedTask;
            }
        }

        public async Task DeleteAsync<T>(string key)
        {
            using (var database = Database())
            {
                var collection = Collection(database);
                var item = collection.FindOne(c => c.Key == key);
                collection.Delete(item.Id);
                await Task.CompletedTask;
            }
        }

        public async Task<T> GetAsync<T>(string key)
        {
            using (var database = Database())
            {
                var completionSource = new TaskCompletionSource<T>();
                var collection = Collection(database);
                var youtubeCredential = collection.FindOne(c => c.Key == key);

                if (youtubeCredential == null || youtubeCredential.Value == null)
                {
                    completionSource.SetResult(default(T));
                }
                else
                {
                    var token = JsonConvert.DeserializeObject<T>(youtubeCredential.Value);
                    completionSource.SetResult(token);
                }

                return await completionSource.Task;
            }
        }

        public async Task StoreAsync<T>(string key, T value)
        {
            using (var database = Database())
            {
                var collection = Collection(database);
                var jsonValue = JsonConvert.SerializeObject(value);

                var youtubeCredential = new YoutubeCredential
                {
                    Key = key,
                    Value = jsonValue
                };

                collection.Upsert(youtubeCredential);
                collection.EnsureIndex(c => c.Key);
                await Task.CompletedTask;
            }
        }

        private ILiteCollection<YoutubeCredential> Collection(LiteDatabase database) => database.GetCollection<YoutubeCredential>("YoutubeCredential");

        private LiteDatabase Database() => new LiteDatabase(@"C:\Users\Stephen\Desktop\SpwnChat\SpwnChat.db");
    }
}