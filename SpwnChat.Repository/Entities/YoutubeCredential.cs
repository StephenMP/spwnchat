﻿namespace SpwnChat.Repository.Entities
{
    public class YoutubeCredential : Entity
    {
        public string Key { get; set; }
        public string Value { get; set; }
    }
}