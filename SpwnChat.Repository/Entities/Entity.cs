﻿namespace SpwnChat.Repository.Entities
{
    public abstract class Entity
    {
        public int Id { get; set; }
    }
}