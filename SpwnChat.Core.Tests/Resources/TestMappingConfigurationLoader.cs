﻿using AutoMapper;
using SpwnChat.Core.Mapping;

namespace SpwnChat.Core.Tests.Resources
{
    public class SecondTestMappingConfigurationLoader : IMappingConfigurationLoader
    {
        public void RegisterMappings(IMapperConfigurationExpression mapperConfiguration)
        {
            mapperConfiguration.CreateMap<TestInnerObject2, TestToInnerObject2>();
            mapperConfiguration.CreateMap<TestFromObject2, TestToObject2>()
                .ForMember(dest => dest.TestToInnerObject2, options => options.MapFrom(src => src.TestInnerObject2));
        }
    }

    public class TestMappingConfigurationLoader : IMappingConfigurationLoader
    {
        public void RegisterMappings(IMapperConfigurationExpression mapperConfiguration)
        {
            mapperConfiguration.CreateMap<TestInnerObject, TestToInnerObject>();
            mapperConfiguration.CreateMap<TestFromObject, TestToObject>()
                .ForMember(dest => dest.TestToInnerObject, options => options.MapFrom(src => src.TestInnerObject));
        }
    }
}