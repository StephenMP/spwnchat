﻿using System;

namespace SpwnChat.Core.Tests.Resources
{
    internal class TestFromObject
    {
        public TestFromObject()
        {
            this.Id = Guid.NewGuid().ToString();
            this.TestInnerObject = new TestInnerObject { Id = Guid.NewGuid().ToString() };
        }

        public string Id { get; set; }

        public TestInnerObject TestInnerObject { get; set; }
    }

    internal class TestFromObject2
    {
        public TestFromObject2()
        {
            this.Id = Guid.NewGuid().ToString();
            this.TestInnerObject2 = new TestInnerObject2 { Id = Guid.NewGuid().ToString() };
        }

        public string Id { get; set; }

        public TestInnerObject2 TestInnerObject2 { get; set; }
    }

    internal class TestInnerObject
    {
        public string Id { get; set; }
    }

    internal class TestInnerObject2
    {
        public string Id { get; set; }
    }

    internal class TestToInnerObject
    {
        public string Id { get; set; }
    }

    internal class TestToInnerObject2
    {
        public string Id { get; set; }
    }

    internal class TestToObject
    {
        public string Id { get; set; }
        public TestInnerObject TestInnerObject { get; set; }
        public TestToInnerObject TestToInnerObject { get; set; }
    }

    internal class TestToObject2
    {
        public string Id { get; set; }
        public TestInnerObject2 TestInnerObject2 { get; set; }
        public TestToInnerObject2 TestToInnerObject2 { get; set; }
    }
}