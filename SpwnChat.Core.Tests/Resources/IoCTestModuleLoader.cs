﻿using LightInject;
using SpwnChat.Core.IoC;

namespace SpwnChat.Core.Tests.Resources
{
    internal class IoCSecondTestModuleLoader : IModuleLoader
    {
        public void RegisterServices(IServiceContainer serviceContainer)
        {
            serviceContainer.Register<ITestInterface, TestInterface>();
            serviceContainer.Register<ISecondTestInterface, SecondTestInterface>();
        }
    }

    internal class IoCTestModuleLoader : IModuleLoader
    {
        public void RegisterServices(IServiceContainer serviceContainer)
        {
            serviceContainer.Register<ITestInterface, TestInterface>();
        }
    }
}