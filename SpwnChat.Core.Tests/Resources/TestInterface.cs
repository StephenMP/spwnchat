﻿namespace SpwnChat.Core.Tests.Resources
{
    public interface ISecondTestInterface
    {
    }

    public interface ITestInterface
    {
    }

    public class SecondTestInterface : ISecondTestInterface
    {
    }

    public class TestInterface : ITestInterface
    {
    }
}