﻿using SpwnChat.Core.Tests.Resources;
using Xunit;
using Mapper = SpwnChat.Core.Mapping.Mapper;

namespace SpwnChat.Core.Tests.Mapping
{
    internal class MappingSteps
    {
        private AutoMapper.IMapper autoMapper;
        private TestMappingConfigurationLoader firstTestMappingConfigurationLoader;
        private Mapper mapper;
        private SecondTestMappingConfigurationLoader secondTestMappingConfigurationLoader;
        private TestFromObject testFromObject;
        private TestFromObject2 testFromObject2;
        private TestToObject testToObject;
        private TestToObject2 testToObject2;

        internal void GivenIHaveASecondTestModuleLoader()
        {
            this.secondTestMappingConfigurationLoader = new SecondTestMappingConfigurationLoader();
        }

        internal void GivenIHaveATestFromObject2ToMap()
        {
            this.testFromObject2 = new TestFromObject2();
        }

        internal void GivenIHaveATestFromObjectToMap()
        {
            this.testFromObject = new TestFromObject();
        }

        internal void GivenIHaveATestMappingConfigurationLoader()
        {
            this.firstTestMappingConfigurationLoader = new TestMappingConfigurationLoader();
        }

        internal void GivenIHaveInitializedTheMapper()
        {
            this.mapper = Mapper.Current;
        }

        internal void ThenIShouldHaveMappedTheTestObjects2Properly()
        {
            Assert.NotNull(this.testToObject2);
            Assert.Equal(this.testFromObject2.Id, this.testToObject2.Id);
            Assert.Equal(this.testFromObject2.TestInnerObject2.Id, this.testToObject2.TestInnerObject2.Id);
            Assert.Equal(this.testFromObject2.TestInnerObject2.Id, this.testToObject2.TestToInnerObject2.Id);
        }

        internal void ThenIShouldHaveMappedTheTestObjectsProperly()
        {
            Assert.NotNull(this.testToObject);
            Assert.Equal(this.testFromObject.Id, this.testToObject.Id);
            Assert.Equal(this.testFromObject.TestInnerObject.Id, this.testToObject.TestInnerObject.Id);
            Assert.Equal(this.testFromObject.TestInnerObject.Id, this.testToObject.TestToInnerObject.Id);
        }

        internal void ThenTheAutoMapperShouldBeInitialized()
        {
            Assert.NotNull(this.autoMapper);
        }

        internal void ThenTheMapperShouldBeInitialized()
        {
            Assert.NotNull(this.mapper);
        }

        internal void WhenIMapATestFromObject2ToATestToObject2()
        {
            this.testToObject2 = this.autoMapper.Map<TestToObject2>(this.testFromObject2);
        }

        internal void WhenIMapATestFromObjectToATestToObject()
        {
            this.testToObject = this.autoMapper.Map<TestToObject>(this.testFromObject);
        }

        internal void WhenIRegisterMySecondTestModuleLoader()
        {
            Mapper.Current.RegisterMappingConfigurations(this.secondTestMappingConfigurationLoader);
        }

        internal void WhenIRegisterMyTestMappingConfiguration()
        {
            Mapper.Current.RegisterMappingConfigurations(this.firstTestMappingConfigurationLoader);
        }

        internal void WhenIResolveTheAutoMapper()
        {
            this.autoMapper = Mapper.AutoMapper;
        }
    }
}