﻿using Xunit;

namespace SpwnChat.Core.Tests.Mapping
{
    public class MappingFeatures
    {
        private readonly MappingSteps steps;

        public MappingFeatures()
        {
            this.steps = new MappingSteps();
        }

        [Fact]
        public void CanRegisterMappings()
        {
            this.steps.GivenIHaveATestMappingConfigurationLoader();
            this.steps.GivenIHaveATestFromObjectToMap();
            this.steps.GivenIHaveInitializedTheMapper();

            this.steps.ThenTheMapperShouldBeInitialized();

            this.steps.WhenIRegisterMyTestMappingConfiguration();
            this.steps.WhenIResolveTheAutoMapper();
            this.steps.WhenIMapATestFromObjectToATestToObject();

            this.steps.ThenTheAutoMapperShouldBeInitialized();
            this.steps.ThenIShouldHaveMappedTheTestObjectsProperly();

            this.steps.GivenIHaveASecondTestModuleLoader();
            this.steps.GivenIHaveATestFromObject2ToMap();

            this.steps.WhenIRegisterMySecondTestModuleLoader();
            this.steps.WhenIResolveTheAutoMapper();
            this.steps.WhenIMapATestFromObject2ToATestToObject2();

            this.steps.ThenIShouldHaveMappedTheTestObjects2Properly();
        }
    }
}