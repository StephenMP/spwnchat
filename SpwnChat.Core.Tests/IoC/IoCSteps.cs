﻿using System;
using SpwnChat.Core.IoC;
using SpwnChat.Core.Tests.Resources;
using Xunit;

namespace SpwnChat.Core.Tests.IoC
{
    internal class IoCSteps : IDisposable
    {
        private bool disposedValue;
        private IoCSecondTestModuleLoader secondTestModuleLoader;
        private IoCTestModuleLoader testModuleLoader;

        public void Dispose()
        {
            Dispose(true);
        }

        internal void GivenIHaveASecondTestModuleLoader()
        {
            this.secondTestModuleLoader = new IoCSecondTestModuleLoader();
        }

        internal void GivenIHaveATestModuleLoader()
        {
            this.testModuleLoader = new IoCTestModuleLoader();
        }

        internal void GivenIHaveInitializedTheServiceLocator()
        {
            var serviceLocator = ServiceLocator.Current;
        }

        internal void ThenIShouldBeAbleToResolveMySecondTestInterface()
        {
            var resolvedItem = ServiceLocator.Current.Resolve<ISecondTestInterface>();
            Assert.NotNull(resolvedItem);
            Assert.IsType<SecondTestInterface>(resolvedItem);
        }

        internal void ThenIShouldBeAbleToResolveMyTestInterface()
        {
            var resolvedItem = ServiceLocator.Current.Resolve<ITestInterface>();
            Assert.NotNull(resolvedItem);
            Assert.IsType<TestInterface>(resolvedItem);
        }

        internal void ThenIShouldNotBeAbleToResolveMySecondTestInterface()
        {
            Assert.Throws<InvalidOperationException>(() => ServiceLocator.Current.Resolve<ISecondTestInterface>());
        }

        internal void ThenTheServiceLocatorShouldBeInitialized()
        {
            Assert.NotNull(ServiceLocator.Current);
        }

        internal void WhenILoadAllAssemblyModules()
        {
            ServiceLocator.Current.RegisterAllAssemblyModules();
        }

        internal void WhenIRegisterMySecondTestModuleLoader()
        {
            ServiceLocator.Current.RegisterModules(this.secondTestModuleLoader);
        }

        internal void WhenIRegisterMyTestModuleLoader()
        {
            ServiceLocator.Current.RegisterModules(this.testModuleLoader);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    ServiceLocator.Current?.Dispose();
                }

                disposedValue = true;
            }
        }
    }
}