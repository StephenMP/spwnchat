﻿using System;
using System.Diagnostics.CodeAnalysis;
using Xunit;

namespace SpwnChat.Core.Tests.IoC
{
    public class IoCFeatures : IDisposable
    {
        private readonly IoCSteps steps;

        private bool disposedValue;

        public IoCFeatures()
        {
            this.steps = new IoCSteps();
        }

        [Fact]
        public void CanRegisterAllAssemblyModules()
        {
            this.steps.GivenIHaveInitializedTheServiceLocator();

            this.steps.WhenILoadAllAssemblyModules();

            this.steps.ThenTheServiceLocatorShouldBeInitialized();
            this.steps.ThenIShouldBeAbleToResolveMyTestInterface();
            this.steps.ThenIShouldBeAbleToResolveMySecondTestInterface();
        }

        [Fact]
        public void CanRegisterSpecificModuleLoaders()
        {
            this.steps.GivenIHaveATestModuleLoader();
            this.steps.GivenIHaveInitializedTheServiceLocator();

            this.steps.WhenIRegisterMyTestModuleLoader();

            this.steps.ThenTheServiceLocatorShouldBeInitialized();
            this.steps.ThenIShouldBeAbleToResolveMyTestInterface();
            this.steps.ThenIShouldNotBeAbleToResolveMySecondTestInterface();
        }

        public void Dispose()
        {
            Dispose(true);
        }

        [SuppressMessage("Microsoft.Usage", "CA2213:DisposableFieldsShouldBeDisposed", MessageId = "steps")]
        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    this.steps?.Dispose();
                }

                disposedValue = true;
            }
        }
    }
}