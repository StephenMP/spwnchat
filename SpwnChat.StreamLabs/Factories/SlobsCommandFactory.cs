﻿using System;
using Newtonsoft.Json;

namespace SpwnChat.StreamLabs.Factories
{
    internal static class SlobsCommandFactory
    {
        private static readonly Random Random = new Random();

        public static string GetActiveScene()
        {
            var command = new
            {
                jsonrpc = "2.0",
                id = Random.Next(),
                method = "activeScene",
                @params = new
                {
                    resource = "ScenesService"
                }
            };

            return JsonConvert.SerializeObject(command);
        }

        public static string GetSource(string sourceId)
        {
            var command = new
            {
                jsonrpc = "2.0",
                id = Random.Next(),
                method = "getSource",
                @params = new
                {
                    resource = "SourcesService",
                    args = new string[] { sourceId }
                }
            };

            return JsonConvert.SerializeObject(command);
        }

        public static string SwitchScenes(string sceneId)
        {
            var command = new
            {
                jsonrpc = "2.0",
                id = Random.Next(),
                method = "makeSceneActive",
                @params = new
                {
                    resource = "ScenesService",
                    args = new string[] { sceneId }
                }
            };

            return JsonConvert.SerializeObject(command);
        }

        internal static string SetSceneItemVisibility(string sceneItemResourceId, bool show)
        {
            var command = new
            {
                jsonrpc = "2.0",
                id = Random.Next(),
                method = "setVisibility",
                @params = new
                {
                    resource = sceneItemResourceId,
                    args = new bool[] { show }
                }
            };

            return JsonConvert.SerializeObject(command);
        }
    }
}