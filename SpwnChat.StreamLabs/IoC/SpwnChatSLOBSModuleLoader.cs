﻿using LightInject;
using SLOBSharp.Client;
using SpwnChat.Core.IoC;
using SpwnChat.StreamLabs.Services;
using StreamLabSharp;

namespace SpwnChat.StreamLabs.IoC
{
    public class SpwnChatSLOBSModuleLoader : IModuleLoader
    {
        public void RegisterServices(IServiceContainer serviceContainer)
        {
            serviceContainer.Register(_ => new Client());
            serviceContainer.Register<ISlobsClient>(_ => new SlobsPipeClient("slobs"));
            serviceContainer.Register<ISlobsService, SlobsService>();
        }
    }
}