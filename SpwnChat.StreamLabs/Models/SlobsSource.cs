﻿namespace SpwnChat.StreamLabs.Models
{
    internal class SlobsSource
    {
        public bool Async { get; set; }
        public bool Audio { get; set; }
        public bool DoNotDuplicate { get; set; }
        public long Height { get; set; }
        public string Id { get; set; }
        public bool Muted { get; set; }
        public string Name { get; set; }
        public string ResourceId { get; set; }
        public string ResultType { get; set; }
        public string SourceId { get; set; }
        public string Type { get; set; }
        public bool Video { get; set; }
        public long Width { get; set; }
    }
}