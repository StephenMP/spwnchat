﻿using System.Collections.Generic;
using SLOBSharp.Client.Responses;

namespace SpwnChat.StreamLabs.Models
{
    internal class SlobsScene
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public List<SlobsNode> Nodes { get; set; }
        public string ResourceId { get; set; }
        public string Type { get; set; }
    }
}