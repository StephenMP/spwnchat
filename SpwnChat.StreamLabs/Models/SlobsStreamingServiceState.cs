﻿using System;

namespace SpwnChat.StreamLabs.Models
{
    public class SlobsStreamingServiceState
    {
        public string RecordingStatus { get; set; }
        public DateTimeOffset RecordingStatusTime { get; set; }
        public string StreamingStatus { get; set; }
        public DateTimeOffset StreamingStatusTime { get; set; }
    }
}