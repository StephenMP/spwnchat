﻿namespace SpwnChat.StreamLabs.Clients
{
    using SpwnChat.StreamLabs.Services;
    using StreamLabSharp;

    public interface IStreamLabsClient
    {
        bool IsConnected { get; }

        void Connect(string streamLabsApiKey, string fileRootPath);

        void Disconnect();
    }

    public class StreamLabsClient : IStreamLabsClient
    {
        private readonly string fileRootPath;
        private readonly ISlobsService slobsService;
        private readonly Client streamLabsSocketClient;

        public StreamLabsClient(Client streamLabsSocketClient, ISlobsService slobsService)
        {
            //this.streamLabsSocketClient = streamLabsSocketClient;
            //this.slobsService = slobsService;
        }

        public bool IsConnected { get; set; }

        public void Connect(string streamLabsApiKey, string fileRootPath)
        {
            this.IsConnected = true;
            //this.fileRootPath = fileRootPath;

            //this.streamLabsSocketClient.OnConnected += this.OnConnected;
            //this.streamLabsSocketClient.OnDisconnected += this.OnDisconnected;

            //this.streamLabsSocketClient.Connect(streamLabsApiKey);
            //while (!this.IsConnected)
            //{
            //    Thread.Sleep(100);
            //}
        }

        public void Disconnect()
        {
            this.IsConnected = false;
            //this.streamLabsSocketClient.Disconnect();
            //while (this.IsConnected)
            //{
            //    Thread.Sleep(100);
            //}

            //this.streamLabsSocketClient.OnConnected -= this.OnConnected;
            //this.streamLabsSocketClient.OnDisconnected -= this.OnDisconnected;
        }

        //private void OnConnected(object sender, bool events)
        //{
        //    this.IsConnected = true;
        //}

        //private void OnDisconnected(object sender, bool events)
        //{
        //    this.IsConnected = false;
        //}
    }
}