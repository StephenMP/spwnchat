﻿using AutoMapper;
using SLOBSharp.Client.Responses;
using SpwnChat.Core.Mapping;
using SpwnChat.StreamLabs.Models;

namespace SpwnChat.StreamLabs.Mapping
{
    public class SpwnChatSLOBSMappingConfigurationLoader : IMappingConfigurationLoader
    {
        public void RegisterMappings(IMapperConfigurationExpression mapperConfiguration)
        {
            mapperConfiguration.CreateMap<SlobsResult, SlobsSource>();
            mapperConfiguration.CreateMap<SlobsResult, SlobsScene>();
            mapperConfiguration.CreateMap<SlobsResult, SlobsStreamingServiceState>();
        }
    }
}