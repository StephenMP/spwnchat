﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Caching;
using System.Threading;
using System.Threading.Tasks;
using Newtonsoft.Json;
using SLOBSharp.Client;
using SLOBSharp.Client.Requests;
using SLOBSharp.Client.Requests.Scenes;
using SLOBSharp.Client.Responses;
using SpwnChat.Core.Mapping;
using SpwnChat.StreamLabs.Models;

namespace SpwnChat.StreamLabs.Services
{
    public interface ISlobsService
    {
        Task<SlobsStreamingServiceState> GetStreamModelAsync();

        Task HideNewFollowerAnimationAsync();

        Task HideNewSubAnimationAsync();

        Task HideNewTipperAnimationAsync();

        bool IsAnimationCommand(string command);

        Task PlayAnimationAsync(string command, string user, bool userIsMod);

        Task PlaySubAnimationAsync(string username);

        Task ShowNewFollowerAnimationAsync();

        Task ShowNewSubAnimationAsync();

        Task ShowNewTipperAnimationAsync();

        Task ShowPieFaceAnimationAsync();

        Task SwitchToLiveSceneAsync();

        Task ToggleSourceAsync(string animationName, bool show);
    }

    public class SlobsService : ISlobsService
    {
        private readonly IDictionary<string, AnimationConfiguration> animationConfigurationsByCommand;
        private readonly ISlobsClient slobsClient;

        public SlobsService(ISlobsClient slobsPipeClient)
        {
            this.animationConfigurationsByCommand = new Dictionary<string, AnimationConfiguration>();
            this.slobsClient = slobsPipeClient;

            this.LoadAnimationConfigs();
        }

        public async Task<SlobsStreamingServiceState> GetStreamModelAsync()
        {
            var request = SlobsRequestBuilder.NewRequest().SetMethod("getModel").SetResource("StreamingService").BuildRequest();
            var response = await this.slobsClient.ExecuteRequestAsync(request).ConfigureAwait(false);
            return Mapper.AutoMapper.Map<SlobsStreamingServiceState>(response.Result.FirstOrDefault());
        }

        public async Task HideNewFollowerAnimationAsync()
        {
            await this.ToggleSourceAsync("Video - Follower Change", false).ConfigureAwait(false);
        }

        public async Task HideNewSubAnimationAsync()
        {
            await this.ToggleSourceAsync("Video - Sub Change", false).ConfigureAwait(false);
        }

        public async Task HideNewTipperAnimationAsync()
        {
            await this.ToggleSourceAsync("Video - Tipper Change", false).ConfigureAwait(false);
        }

        public bool IsAnimationCommand(string command)
        {
            return this.animationConfigurationsByCommand.ContainsKey(command);
        }

        public async Task PlayAnimationAsync(string command, string user, bool userIsMod)
        {
            if (this.animationConfigurationsByCommand.ContainsKey(command))
            {
                var activeScene = await GetActiveSceneAsync().ConfigureAwait(false);
                if (activeScene != null)
                {
                    var activeSources = await GetActiveSceneItemSourcesAsync(activeScene).ConfigureAwait(false);
                    if (activeSources != null)
                    {
                        var animationConfig = this.animationConfigurationsByCommand[command];
                        var showRequests = new List<ISlobsRequest>();
                        var hideRequests = new List<ISlobsRequest>();

                        foreach (var config in animationConfig.AnimationResources)
                        {
                            var animationSource = activeSources.FirstOrDefault(source => source.Name == config);
                            if (animationSource != null)
                            {
                                var sceneItem = activeScene.Nodes.Find(node => node.SourceId == animationSource.SourceId);
                                if (sceneItem != null)
                                {
                                    sceneItem.ResourceId = $"SceneItem[\"{sceneItem.SceneId}\",\"{sceneItem.SceneItemId}\",\"{sceneItem.SourceId}\"]";
                                    var showCommand = SlobsRequestBuilder.NewRequest().SetMethod("setVisibility").SetResource(sceneItem.ResourceId).AddArgs(true).BuildRequest();
                                    var hideCommand = SlobsRequestBuilder.NewRequest().SetMethod("setVisibility").SetResource(sceneItem.ResourceId).AddArgs(false).BuildRequest();
                                    showRequests.Add(showCommand);
                                    hideRequests.Add(hideCommand);
                                }
                            }
                        }

                        await this.slobsClient.ExecuteRequestsAsync(showRequests).ConfigureAwait(false);

                        Thread.Sleep(animationConfig.AnimationDelay);

                        await this.slobsClient.ExecuteRequestsAsync(hideRequests).ConfigureAwait(false);

                        PutAnimationOnCooldown(command, user, animationConfig.CooldownMilliseconds);
                    }
                }
            }
        }

        public async Task PlaySubAnimationAsync(string username)
        {
            var text = $"{username}\nis soooooooo\nAWESOME!";
            File.WriteAllText(@"C:\Users\Stephen\Documents\Streaming\Labels\sub_animation.txt", text);
            await Task.Delay(1000);

            await this.ToggleSourceAsync("Video - Sub Animation", true);
            await Task.Delay(1000);
            await this.ToggleSourceAsync("Text - Sub Animation", true);
            await Task.Delay(9000);
            await this.ToggleSourceAsync("Video - Sub Animation", false);
            await this.ToggleSourceAsync("Text - Sub Animation", false);
        }

        public async Task ShowNewFollowerAnimationAsync()
        {
            await this.ToggleSourceAsync("Video - Follower Change", true).ConfigureAwait(false);
        }

        public async Task ShowNewSubAnimationAsync()
        {
            await this.ToggleSourceAsync("Video - Sub Change", true).ConfigureAwait(false);
        }

        public async Task ShowNewTipperAnimationAsync()
        {
            await this.ToggleSourceAsync("Video - Tipper Change", true).ConfigureAwait(false);
        }

        public async Task ShowPieFaceAnimationAsync()
        {
            await this.ToggleSourceAsync("Video - Pie Face", true);
            await Task.Delay(3000);
            await this.ToggleSourceAsync("Video - Pie Face", false);
        }

        public async Task SwitchToLiveSceneAsync()
        {
            var activeSceneResponse = await GetActiveSceneAsync().ConfigureAwait(false);
            if (activeSceneResponse.Name == "Intermission")
            {
                var scenes = await GetScenesAsync().ConfigureAwait(false);
                var liveScene = scenes.FirstOrDefault(scene => scene.Name == "Live");

                if (liveScene != null)
                {
                    var request = SlobsRequestBuilder.NewRequest().SetMethod("makeSceneActive").SetResource("ScenesService").AddArgs(liveScene.Id).BuildRequest();
                    await this.slobsClient.ExecuteRequestAsync(request).ConfigureAwait(false);
                }
            }
        }

        public async Task ToggleSourceAsync(string animationName, bool show)
        {
            var activeScene = await GetActiveSceneAsync().ConfigureAwait(false);
            if (activeScene != null)
            {
                var activeSources = await GetActiveSceneItemSourcesAsync(activeScene).ConfigureAwait(false);
                if (activeSources != null)
                {
                    var animationSource = activeSources.FirstOrDefault(source => source.Name == animationName);
                    if (animationSource != null)
                    {
                        var sceneItem = activeScene.Nodes.Find(node => node.SourceId == animationSource.SourceId);
                        if (sceneItem != null)
                        {
                            sceneItem.ResourceId = $"SceneItem[\"{sceneItem.SceneId}\",\"{sceneItem.SceneItemId}\",\"{sceneItem.SourceId}\"]";
                            var request = SlobsRequestBuilder.NewRequest().SetMethod("setVisibility").SetResource(sceneItem.ResourceId).AddArgs(show).BuildRequest();
                            await this.slobsClient.ExecuteRequestAsync(request).ConfigureAwait(false);
                        }
                    }
                }
            }
        }

        private static void PutAnimationOnCooldown(string command, string user, long cooldownMilliseconds)
        {
            if (!string.Equals(user, "mrspwn", StringComparison.OrdinalIgnoreCase))
            {
                MemoryCache.Default.Add(command, command, DateTime.Now.AddMilliseconds(cooldownMilliseconds));
            }
        }

        private async Task<SlobsScene> GetActiveSceneAsync()
        {
            var request = new SlobsGetActiveSceneRequest();
            var activeSceneResponse = await this.slobsClient.ExecuteRequestAsync(request).ConfigureAwait(false);
            return Mapper.AutoMapper.Map<SlobsScene>(activeSceneResponse.Result.FirstOrDefault());
        }

        private Task<IEnumerable<SlobsSource>> GetActiveSceneItemSourcesAsync(SlobsScene activeScene)
        {
            var keySuffix = $"{nameof(GetActiveSceneItemSourcesAsync)}:{activeScene.Id}";
            return this.GetOrSetCachedAsync<IEnumerable<SlobsSource>>(keySuffix, async () =>
            {
                var itemNodes = activeScene.Nodes.Where(node => node.SceneNodeType == SceneNodeType.Item);
                var commands = new List<ISlobsRequest>();
                var activeSources = new List<SlobsSource>();

                foreach (var node in itemNodes)
                {
                    var request = SlobsRequestBuilder.NewRequest().SetMethod("getSource").SetResource("SourcesService").AddArgs(node.SourceId).BuildRequest();
                    commands.Add(request);
                }

                foreach (var activeSourcesResponse in await this.slobsClient.ExecuteRequestsAsync(commands).ConfigureAwait(false))
                {
                    var slobsSource = Mapper.AutoMapper.Map<SlobsSource>(activeSourcesResponse.Result.FirstOrDefault());
                    activeSources.Add(slobsSource);
                }

                return activeSources;
            });
        }

        private async Task<TResult> GetOrSetCachedAsync<TResult>(string keySuffix, Func<Task<TResult>> callback) where TResult : class
        {
            var key = $"{nameof(SlobsService)}:{keySuffix}";
            if (MemoryCache.Default.Contains(key))
            {
                return MemoryCache.Default[key] as TResult;
            }

            var itemToCache = await callback().ConfigureAwait(false);
            MemoryCache.Default.Set(key, itemToCache, DateTime.Now.AddHours(1));

            return itemToCache;
        }

        private async Task<IEnumerable<SlobsScene>> GetScenesAsync()
        {
            var request = SlobsRequestBuilder.NewRequest().SetMethod("scenes").SetResource("ScenesService").BuildRequest();
            var scenesResponse = await this.slobsClient.ExecuteRequestAsync(request).ConfigureAwait(false);
            return Mapper.AutoMapper.Map<List<SlobsScene>>(scenesResponse.Result);
        }

        private void LoadAnimationConfigs()
        {
            const string animationsConfigPath = @"C:\Users\Stephen\Desktop\SpwnChat\animations.json";
            var animationConfigsJson = File.ReadAllText(animationsConfigPath);
            var animationConfigs = JsonConvert.DeserializeObject<List<AnimationConfiguration>>(animationConfigsJson);
            foreach (var config in animationConfigs)
            {
                this.animationConfigurationsByCommand.Add(config.AnimationCommand, config);
            }
        }
    }
}