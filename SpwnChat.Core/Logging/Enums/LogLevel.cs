﻿namespace SpwnChat.Core.Logging.Enums
{
    public enum LogLevel
    {
        Info,
        Error
    }
}