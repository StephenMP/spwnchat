﻿using System;
using Newtonsoft.Json;
using Serilog;
using SpwnChat.Core.Logging.Enums;

namespace SpwnChat.Core.Logging
{
    public interface ILogger
    {
        void CloseAndFlush();

        void Error(object sender, string message, object data = null);

        void Info(object sender, string message, object data = null);
    }

    public class Logger : ILogger
    {
        private static bool loggerInitialized;

        public static void WriteLog(string sender, LogLevel logLevel, string message, object data = null)
        {
            if (!loggerInitialized)
            {
                var logFileName = $"C:\\Users\\Stephen\\Desktop\\SpwnChat\\Logs\\{DateTime.Now.ToLongDateString()}.txt";

#if DEBUG
                logFileName = $"C:\\Users\\Stephen\\Desktop\\SpwnChat\\Logs\\{DateTime.Now.ToLongDateString()}.DEBUG.txt";
#endif

                Log.Logger = new LoggerConfiguration().WriteTo.Async(a => a.File(logFileName)).CreateLogger();
                loggerInitialized = true;
            }

            var logMessage = $"- {sender} - {message}";
            if (data != null)
            {
                var json = JsonConvert.SerializeObject(data);
                logMessage = $"{logMessage} | {json}";
            }

            if (logLevel == LogLevel.Info)
            {
                Log.Logger.Information(logMessage);
            }
            else if (logLevel == LogLevel.Error)
            {
                Log.Logger.Error(logMessage);
            }
        }

        public void CloseAndFlush()
        {
            Log.CloseAndFlush();
        }

        public void Error(object sender, string message, object data = null)
        {
            WriteLog(sender.GetType().Name, LogLevel.Error, message, data);
        }

        public void Info(object sender, string message, object data = null)
        {
            Logger.WriteLog(sender.GetType().Name, LogLevel.Info, message, data);
        }
    }
}