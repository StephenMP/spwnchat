﻿using LightInject;

namespace SpwnChat.Core.IoC
{
    public interface IModuleLoader
    {
        void RegisterServices(IServiceContainer serviceContainer);
    }
}