﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Reflection;
using LightInject;

namespace SpwnChat.Core.IoC
{
    public interface IServiceLocator : IDisposable
    {
        void RegisterAllAssemblyModules();

        void RegisterModules(params IModuleLoader[] moduleLoaders);

        T Resolve<T>();
    }

    public class ServiceLocator : IServiceLocator
    {
        private static IServiceLocator current;
        private readonly HashSet<string> loadedAssemblyModules;
        private readonly IServiceContainer serviceContainer;
        private bool disposedValue;

        public ServiceLocator()
        {
            AppDomain.CurrentDomain.AssemblyLoad += AssemblyLoaded;

            this.loadedAssemblyModules = new HashSet<string>();
            this.serviceContainer = new ServiceContainer();
        }

        public static IServiceLocator Current
        {
            get
            {
                current = current ?? new ServiceLocator();
                return current;
            }
        }

        public void Dispose()
        {
            this.Dispose(true);
            current = null;
        }

        public void RegisterAllAssemblyModules()
        {
            var loadedAssemblies = AppDomain.CurrentDomain.GetAssemblies().Where(a => a.FullName.Contains("SpwnChat"));
            foreach (var loadedAssembly in loadedAssemblies)
            {
                this.RegisterAssembly(loadedAssembly);
            }
        }

        public void RegisterModules(params IModuleLoader[] moduleLoaders)
        {
            foreach (var moduleLoader in moduleLoaders)
            {
                moduleLoader.RegisterServices(this.serviceContainer);
            }
        }

        public T Resolve<T>()
        {
            return this.serviceContainer.GetInstance<T>();
        }

        [SuppressMessage("Microsoft.Usage", "CA2213:DisposableFieldsShouldBeDisposed", MessageId = "serviceContainer")]
        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    this.serviceContainer?.Dispose();
                }

                this.disposedValue = true;
            }
        }

        private void AssemblyLoaded(object sender, AssemblyLoadEventArgs args)
        {
            this.RegisterAssembly(args.LoadedAssembly);
        }

        private IEnumerable<IModuleLoader> GetModuleLoaders(Assembly loadedAssembly)
        {
            return loadedAssembly.GetTypes()
                                 .Where(t => t.GetInterfaces().Contains(typeof(IModuleLoader)) && t.GetConstructor(Type.EmptyTypes) != null)
                                 .Select(t => Activator.CreateInstance(t) as IModuleLoader);
        }

        private void RegisterAssembly(Assembly assembly)
        {
            if (assembly.FullName.Contains("SpwnChat"))
            {
                if (!this.loadedAssemblyModules.Contains(assembly.FullName))
                {
                    var moduleLoaders = GetModuleLoaders(assembly);
                    this.RegisterModules(moduleLoaders.ToArray());

                    this.loadedAssemblyModules.Add(assembly.FullName);
                }
            }
        }
    }
}