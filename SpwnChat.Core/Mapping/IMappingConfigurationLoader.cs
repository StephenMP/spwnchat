﻿using AutoMapper;

namespace SpwnChat.Core.Mapping
{
    public interface IMappingConfigurationLoader
    {
        void RegisterMappings(IMapperConfigurationExpression mapperConfiguration);
    }
}