﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using AutoMapper;
using AutoMapper.Configuration;

namespace SpwnChat.Core.Mapping
{
    public interface IMapper
    {
        AutoMapper.IMapper InternalMapper { get; }

        void RegisterAllAssemblyMappingConfigurations();

        void RegisterMappingConfigurations(params IMappingConfigurationLoader[] moduleLoaders);
    }

    public class Mapper : IMapper
    {
        private static Mapper current;
        private readonly HashSet<string> loadedAssemblyModules;
        private readonly IMapperConfigurationExpression mapperConfiguration;

        public Mapper()
        {
            AppDomain.CurrentDomain.AssemblyLoad += AssemblyLoaded;

            this.loadedAssemblyModules = new HashSet<string>();
            this.mapperConfiguration = new MapperConfigurationExpression();
            this.InternalMapper = new MapperConfiguration(_ => { }).CreateMapper();
        }

        public static AutoMapper.IMapper AutoMapper
        {
            get
            {
                current = current ?? new Mapper();
                return current.InternalMapper;
            }
        }

        public static Mapper Current
        {
            get
            {
                current = current ?? new Mapper();
                return current;
            }
        }

        public AutoMapper.IMapper InternalMapper { get; private set; }

        public void RegisterAllAssemblyMappingConfigurations()
        {
            var loadedAssemblies = AppDomain.CurrentDomain.GetAssemblies().Where(a => a.FullName.Contains("SpwnChat"));
            foreach (var loadedAssembly in loadedAssemblies)
            {
                this.RegisterAssembly(loadedAssembly);
            }
        }

        public void RegisterMappingConfigurations(params IMappingConfigurationLoader[] moduleLoaders)
        {
            foreach (var moduleLoader in moduleLoaders)
            {
                moduleLoader.RegisterMappings(this.mapperConfiguration);
                var mappingConfig = new MapperConfiguration((MapperConfigurationExpression)this.mapperConfiguration);
                this.InternalMapper = new AutoMapper.Mapper(mappingConfig);
            }
        }

        private void AssemblyLoaded(object sender, AssemblyLoadEventArgs args)
        {
            this.RegisterAssembly(args.LoadedAssembly);
        }

        private IEnumerable<IMappingConfigurationLoader> GetModuleLoaders(Assembly loadedAssembly)
        {
            var moduleLoaders = loadedAssembly.GetTypes()
                                              .Where(t => t.GetInterfaces().Contains(typeof(IMappingConfigurationLoader)) && t.GetConstructor(Type.EmptyTypes) != null)
                                              .Select(t => Activator.CreateInstance(t) as IMappingConfigurationLoader);

            return moduleLoaders;
        }

        private void RegisterAssembly(Assembly assembly)
        {
            if (assembly.FullName.Contains("SpwnChat"))
            {
                if (!this.loadedAssemblyModules.Contains(assembly.FullName))
                {
                    var moduleLoaders = GetModuleLoaders(assembly);
                    this.RegisterMappingConfigurations(moduleLoaders.ToArray());

                    this.loadedAssemblyModules.Add(assembly.FullName);
                }
            }
        }
    }
}