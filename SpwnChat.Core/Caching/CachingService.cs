﻿using System;
using System.Runtime.Caching;

namespace SpwnChat.Core.Caching
{
    public interface ICachingService
    {
        void CacheItem(string key, object itemToCache, DateTimeOffset expiration);

        bool Contains(string key);

        object GetItem(string key);
    }

    public class CachingService : ICachingService
    {
        public void CacheItem(string key, object itemToCache, DateTimeOffset expiration)
        {
            MemoryCache.Default.Add(key, itemToCache, expiration);
        }

        public bool Contains(string key)
        {
            return MemoryCache.Default.Contains(key);
        }

        public object GetItem(string key)
        {
            return MemoryCache.Default.Get(key);
        }
    }
}