﻿using System;
using System.Diagnostics.CodeAnalysis;
using System.Drawing;
using Xunit;

namespace SpwnChat.UI.Tests.Forms
{
    public class MainFormFeatures : IDisposable
    {
        private readonly MainFormSteps steps;

        private bool disposedValue;

        public MainFormFeatures()
        {
            this.steps = new MainFormSteps();
        }

        [Fact]
        public void CanBuildMainForm()
        {
            this.steps.GivenIHaveAMockedStreamlabsSocketClient();
            this.steps.GivenIHaveAMockedDiscordService();
            this.steps.GivenIHaveAMockedChatBotClient();
            this.steps.GivenIHaveAMockedLogger();

            this.steps.WhenICreateTheMainForm();

            this.steps.ThenTheMainFormShouldNotBeNull();
            this.steps.ThenTheMainFormShouldHaveAllComponentsInitialized();
        }

        [Fact]
        public void CanHandleCloseFormWithEverythingConnected()
        {
            this.steps.GivenIHaveAMainForm();

            this.steps.WhenIClickConnectEverythingButton();
            this.steps.WhenICloseMainForm();
        }

        [Fact]
        public void CanHandleCloseFormWithNothingConnected()
        {
            this.steps.GivenIHaveAMainForm();

            this.steps.WhenICloseMainForm();

            this.steps.ThenTheConnectBotButtonShouldChangeTextTo("Connect Bot");
            this.steps.ThenTheConnectLabelsButtonShouldChangeTextTo("Connect Labels");
            this.steps.ThenTheAnnounceLiveOnDiscordButtonShouldChangeTextTo("Announce Live in Discord");
            this.steps.ThenTheConnectEverythingButtonShouldChangeTextTo("Connect Everything");
            this.steps.ThenTheConnectEverythingButtonShouldBeEnabled();

            this.steps.ThenTheAnnounceLiveOnDiscordButtonColorShouldBe(SystemColors.Control);
            this.steps.ThenTheConnectBotButtonColorShouldBe(SystemColors.Control);
            this.steps.ThenTheConnectEverythingButtonColorShouldBe(SystemColors.Control);
            this.steps.ThenTheConnectLabelsButtonColorShouldBe(SystemColors.Control);
        }

        [Fact]
        public void CanToggleMainformAnnounceLiveOnDiscordButtonProperly()
        {
            this.steps.GivenIHaveAMainForm();

            this.steps.WhenIClickAnnounceLiveOnDiscordButton();

            this.steps.ThenTheAnnounceLiveOnDiscordButtonShouldChangeTextTo("Delete Announcement in Discord");
            this.steps.ThenTheAnnounceLiveOnDiscordButtonColorShouldBe(Color.LightGreen);
            this.steps.ThenTheConnectEverythingButtonShouldBeDisabled();

            this.steps.WhenIClickAnnounceLiveOnDiscordButton();

            this.steps.ThenTheAnnounceLiveOnDiscordButtonShouldChangeTextTo("Announce Live in Discord");
            this.steps.ThenTheAnnounceLiveOnDiscordButtonColorShouldBe(SystemColors.Control);
            this.steps.ThenTheConnectEverythingButtonShouldBeEnabled();
        }

        [Fact]
        public void CanToggleMainformConnectBotButtonProperly()
        {
            this.steps.GivenIHaveAMainForm();

            this.steps.WhenIClickConnectBotButton();

            this.steps.ThenTheConnectBotButtonShouldChangeTextTo("Disconnect Bot");
            this.steps.ThenTheConnectBotButtonColorShouldBe(Color.LightGreen);
            this.steps.ThenTheConnectEverythingButtonShouldBeDisabled();

            this.steps.WhenIClickConnectBotButton();

            this.steps.ThenTheConnectBotButtonShouldChangeTextTo("Connect Bot");
            this.steps.ThenTheConnectBotButtonColorShouldBe(SystemColors.Control);
            this.steps.ThenTheConnectEverythingButtonShouldBeEnabled();
        }

        [Fact]
        public void CanToggleMainformConnectEverythingButtonProperly()
        {
            this.steps.GivenIHaveAMainForm();

            this.steps.WhenIClickConnectEverythingButton();

            this.steps.ThenTheConnectEverythingButtonShouldChangeTextTo("Disconnect Everything");
            this.steps.ThenTheConnectEverythingButtonColorShouldBe(SystemColors.Control);
            this.steps.ThenTheConnectEverythingButtonShouldBeEnabled();
            this.steps.ThenTheConnectBotButtonShouldChangeTextTo("Disconnect Bot");
            this.steps.ThenTheConnectBotButtonColorShouldBe(Color.LightGreen);
            this.steps.ThenTheConnectLabelsButtonShouldChangeTextTo("Disconnect Labels");
            this.steps.ThenTheConnectLabelsButtonColorShouldBe(Color.LightGreen);
            this.steps.ThenTheAnnounceLiveOnDiscordButtonShouldChangeTextTo("Delete Announcement in Discord");
            this.steps.ThenTheAnnounceLiveOnDiscordButtonColorShouldBe(Color.LightGreen);

            this.steps.WhenIClickConnectEverythingButton();

            this.steps.ThenTheConnectEverythingButtonShouldChangeTextTo("Connect Everything");
            this.steps.ThenTheConnectEverythingButtonColorShouldBe(SystemColors.Control);
            this.steps.ThenTheConnectEverythingButtonShouldBeEnabled();
            this.steps.ThenTheConnectBotButtonShouldChangeTextTo("Connect Bot");
            this.steps.ThenTheConnectBotButtonColorShouldBe(SystemColors.Control);
            this.steps.ThenTheConnectLabelsButtonShouldChangeTextTo("Connect Labels");
            this.steps.ThenTheConnectLabelsButtonColorShouldBe(SystemColors.Control);
            this.steps.ThenTheAnnounceLiveOnDiscordButtonShouldChangeTextTo("Announce Live in Discord");
            this.steps.ThenTheAnnounceLiveOnDiscordButtonColorShouldBe(SystemColors.Control);
        }

        [Fact]
        public void CanToggleMainformConnectLabelsButtonProperly()
        {
            this.steps.GivenIHaveAMainForm();

            this.steps.WhenIClickConnectLabelsButton();

            this.steps.ThenTheConnectLabelsButtonShouldChangeTextTo("Disconnect Labels");
            this.steps.ThenTheConnectLabelsButtonColorShouldBe(Color.LightGreen);
            this.steps.ThenTheConnectEverythingButtonShouldBeDisabled();

            this.steps.WhenIClickConnectLabelsButton();

            this.steps.ThenTheConnectLabelsButtonShouldChangeTextTo("Connect Labels");
            this.steps.ThenTheConnectLabelsButtonColorShouldBe(SystemColors.Control);
            this.steps.ThenTheConnectEverythingButtonShouldBeEnabled();
        }

        public void Dispose()
        {
            Dispose(true);
        }

        [SuppressMessage("Microsoft.Usage", "CA2213:DisposableFieldsShouldBeDisposed", MessageId = "steps")]
        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    this.steps?.Dispose();
                }

                disposedValue = true;
            }
        }
    }
}