﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Threading.Tasks;
using System.Windows.Forms;
using Discord;
using Moq;
using SpwnChat.ChatClient;
using SpwnChat.Core.Logging;
using SpwnChat.Discord.Services;
using SpwnChat.StreamLabs.Clients;
using SpwnChat.UI.Forms;
using Xunit;
using Color = System.Drawing.Color;

namespace SpwnChat.UI.Tests.Forms
{
    internal class MainFormSteps : IDisposable
    {
        private bool disposedValue;
        private MainForm mainForm;
        private Mock<IChatBotClient> mockedChatBotClient;
        private Mock<IDiscordService> mockedDiscordService;
        private Mock<ILogger> mockedLogger;
        private Mock<IStreamLabsClient> mockedStreamlabsSocketClient;

        public void Dispose()
        {
            Dispose(true);
        }

        internal void GivenIHaveAMainForm()
        {
            this.GivenIHaveAMockedStreamlabsSocketClient();
            this.GivenIHaveAMockedDiscordService();
            this.GivenIHaveAMockedChatBotClient();
            this.GivenIHaveAMockedLogger();
            this.mainForm = new MainForm(this.mockedDiscordService.Object, this.mockedChatBotClient.Object, null, this.mockedLogger.Object);
        }

        internal void GivenIHaveAMockedChatBotClient()
        {
            this.mockedChatBotClient = new Mock<IChatBotClient>();
            this.mockedChatBotClient.Setup(x => x.Connect()).Callback(() => this.mockedChatBotClient.SetupGet(y => y.IsConnected).Returns(true)).Verifiable();
            this.mockedChatBotClient.Setup(x => x.Disconnect()).Callback(() => this.mockedChatBotClient.SetupGet(y => y.IsConnected).Returns(false)).Verifiable();
        }

        internal void GivenIHaveAMockedDiscordService()
        {
            this.mockedDiscordService = new Mock<IDiscordService>();
            this.mockedDiscordService.Setup(x => x.LoginAsync()).Callback(() => this.mockedDiscordService.SetupGet(y => y.IsConnected).Returns(true)).Returns(Task.CompletedTask);
            this.mockedDiscordService.Setup(x => x.LogoutAsync()).Callback(() => this.mockedDiscordService.SetupGet(y => y.IsConnected).Returns(false)).Returns(Task.CompletedTask);
            this.mockedDiscordService.Setup(x => x.PublishGoLiveAsync(It.IsAny<string>())).Returns(Task.FromResult((IMessage)new TestDiscordMessage()));
        }

        internal void GivenIHaveAMockedLogger()
        {
            this.mockedLogger = new Mock<ILogger>();
        }

        internal void GivenIHaveAMockedStreamlabsSocketClient()
        {
            this.mockedStreamlabsSocketClient = new Mock<IStreamLabsClient>();
            this.mockedStreamlabsSocketClient.Setup(x => x.Connect(It.IsAny<string>(), It.IsAny<string>())).Callback(() => this.mockedStreamlabsSocketClient.SetupGet(y => y.IsConnected).Returns(true));
            this.mockedStreamlabsSocketClient.Setup(x => x.Disconnect()).Callback(() => this.mockedStreamlabsSocketClient.SetupGet(y => y.IsConnected).Returns(false));
        }

        internal void ThenTheAnnounceLiveOnDiscordButtonColorShouldBe(Color color)
        {
            Assert.Equal(color, this.mainForm.btnDiscordAnnounceLive.BackColor);
        }

        internal void ThenTheAnnounceLiveOnDiscordButtonShouldChangeTextTo(string text)
        {
            Assert.Equal(text, this.mainForm.btnDiscordAnnounceLive.Text);
        }

        internal void ThenTheConnectBotButtonColorShouldBe(Color color)
        {
            Assert.Equal(color, this.mainForm.btnBotConnect.BackColor);
        }

        internal void ThenTheConnectBotButtonShouldChangeTextTo(string text)
        {
            Assert.Equal(text, this.mainForm.btnBotConnect.Text);
        }

        internal void ThenTheConnectEverythingButtonColorShouldBe(Color color)
        {
            Assert.Equal(color, this.mainForm.btnEverythingConnect.BackColor);
        }

        internal void ThenTheConnectEverythingButtonShouldBeDisabled()
        {
            Assert.False(this.mainForm.btnEverythingConnect.Enabled);
        }

        internal void ThenTheConnectEverythingButtonShouldBeEnabled()
        {
            Assert.True(this.mainForm.btnEverythingConnect.Enabled);
        }

        internal void ThenTheConnectEverythingButtonShouldChangeTextTo(string text)
        {
            Assert.Equal(text, this.mainForm.btnEverythingConnect.Text);
        }

        internal void ThenTheConnectLabelsButtonColorShouldBe(Color color)
        {
            Assert.Equal(color, this.mainForm.btnOBSConnect.BackColor);
        }

        internal void ThenTheConnectLabelsButtonShouldChangeTextTo(string text)
        {
            Assert.Equal(text, this.mainForm.btnOBSConnect.Text);
        }

        internal void ThenTheMainFormShouldHaveAllComponentsInitialized()
        {
            var controls = this.mainForm.Controls;
            foreach (var control in controls)
            {
                Assert.NotNull(control);
            }
        }

        internal void ThenTheMainFormShouldNotBeNull()
        {
            Assert.NotNull(this.mainForm);
        }

        internal void WhenIClickAnnounceLiveOnDiscordButton()
        {
            this.mainForm.HandleClickEvent(this.mainForm.btnDiscordAnnounceLive, null);
        }

        internal void WhenIClickConnectBotButton()
        {
            this.mainForm.HandleClickEvent(this.mainForm.btnBotConnect, null);
        }

        internal void WhenIClickConnectEverythingButton()
        {
            this.mainForm.HandleClickEvent(this.mainForm.btnEverythingConnect, null);
        }

        internal void WhenIClickConnectLabelsButton()
        {
            this.mainForm.HandleClickEvent(this.mainForm.btnOBSConnect, null);
        }

        internal void WhenICloseMainForm()
        {
            this.mainForm.HandleFormClosing(this, new FormClosingEventArgs(CloseReason.None, false));
        }

        internal void WhenICreateTheMainForm()
        {
            this.mainForm = new MainForm(this.mockedDiscordService.Object, this.mockedChatBotClient.Object, null, this.mockedLogger.Object);
        }

        [SuppressMessage("Microsoft.Usage", "CA2213:DisposableFieldsShouldBeDisposed", MessageId = "mainForm")]
        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    this.mainForm?.Dispose();
                }

                disposedValue = true;
            }
        }
    }

    //ncrunch: no coverage start
    internal class TestDiscordMessage : IMessage
    {
#pragma warning disable RCS1079 // Throwing of new NotImplementedException.
        public IReadOnlyCollection<IAttachment> Attachments => throw new NotImplementedException();
        public IUser Author => throw new NotImplementedException();
        public IMessageChannel Channel => throw new NotImplementedException();
        public string Content => throw new NotImplementedException();
        public DateTimeOffset CreatedAt => throw new NotImplementedException();
        public DateTimeOffset? EditedTimestamp => throw new NotImplementedException();
        public IReadOnlyCollection<IEmbed> Embeds => throw new NotImplementedException();
        public ulong Id => throw new NotImplementedException();
        public bool IsPinned => throw new NotImplementedException();
        public bool IsTTS => throw new NotImplementedException();
        public IReadOnlyCollection<ulong> MentionedChannelIds => throw new NotImplementedException();
        public IReadOnlyCollection<ulong> MentionedRoleIds => throw new NotImplementedException();
        public IReadOnlyCollection<ulong> MentionedUserIds => throw new NotImplementedException();
        public MessageSource Source => throw new NotImplementedException();
        public IReadOnlyCollection<ITag> Tags => throw new NotImplementedException();
        public DateTimeOffset Timestamp => throw new NotImplementedException();
        public MessageType Type => throw new NotImplementedException();
        public MessageActivity Activity => throw new NotImplementedException();
        public MessageApplication Application => throw new NotImplementedException();

        public Task DeleteAsync(RequestOptions options = null)
        {
            throw new NotImplementedException();
        }

#pragma warning restore RCS1079 // Throwing of new NotImplementedException.
    }

    //ncrunch: no coverage end
}